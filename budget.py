class Category:

  def __init__(self, name):
    self.name = name
    self.ledger = []

  def __repr__(self):
    statement = str()
    # A title line of 30 characters where the name of the category is centered in a line of `*` characters.
    left = int((30-len(self.name))/2)
    right = 30 - left - len(self.name) 
    leftstr = str()
    for i in range(left):
      leftstr = leftstr + '*'
    rightstr = str()
    for i in range(right):
      rightstr = rightstr + '*'
    rightstr = rightstr + '\n'
    statement = statement + leftstr + self.name + rightstr
    # A list of the items in the ledger. Each line should show the description and amount. The first 23 characters of the 
    # description should be displayed, then the amount. The amount should be right aligned, contain two decimal places, and display # a maximum of 7 characters.
    for item in self.ledger:
      number = item['amount']
      number = "{:.2f}".format(number)
      newstr = item['description']
      if len(newstr) > 23:
        newstr = item['description'][0:22]
      if len(newstr) < 23:
        add_spaces = 23 - len(newstr)
        for i in range(add_spaces-1):
          newstr = newstr + ' '
      numstr = str(number)
      if len(numstr) == 7:
        newnum = numstr
      if len(numstr) < 7:
        newnum = numstr.rjust(7)   
      newline = newstr + ' ' + newnum + '\n'
      statement = statement + newline 
    # A line displaying the category total.
    statement = statement + 'Total: ' + str("{:.2f}".format(self.get_balance()))
    return statement    

  def deposit(self, amount, description=""):
    self.amount = amount
    self.description = description
    self.ledger.append({'amount':amount, 'description':description})
  
  def withdraw(self, amount, description=""):
    self.amount = amount
    self.description = description
    if self.check_funds(amount): 
      self.ledger.append({'amount':-amount, 'description':description})
    
  def get_balance(self):
    sum = 0
    for item in self.ledger:
      sum = sum + item['amount']
    return sum
    
  def transfer(self, amount, other_category):
    if self.check_funds(amount):
      description = "Transfer to " + other_category.name
      self.ledger.append({'amount':-amount, 'description':description})
      description = "Transfer from " + self.name
      other_category.ledger.append({'amount':amount, 'description':description})
      return True
    return False
  
  def check_funds(self, amount):
    if self.get_balance() < amount:
      return False
    return True






def create_spend_chart(list_args):

  spending_list = []
  total_spending = 0
  expense_list = []

  for item in list_args:
    expense_list.append(item.name)  
    item_spending = 0
    for item1 in item.ledger:      
      if item1['amount'] > 0:
        item_spending = item_spending + item1['amount']
    total_spending = total_spending + item_spending
    spending_list.append({'name':item.name, 'spending':item_spending})    
  #print(spending_list)

  spending_percentage = []
  for item in spending_list:
    item_prop = round(10*item['spending']/total_spending)
    spending_percentage.append({'name':item['name'],'spending ratio':item_prop})
  #print(spending_percentage)
  print('Percentage spent by category')
  for i in range(11):
    stri = str(10*(10-i))
    if i >= 1:
      stri = ' ' + stri 
    if i == 10:
      stri = ' ' + stri
    stri = stri + '|'
    for item1 in spending_percentage:
      if item1['spending ratio'] >= 10 - i:
        stri = stri + ' ' + 'o'
      else:
        stri = stri + ' ' + ' '
    print(stri)
    stri = '    -'
  for item1 in spending_percentage:  
    stri = stri + '---'
  print(stri)  


  vert_size = 0
  for item2 in expense_list:
    if vert_size < len(item2):
      vert_size = len(item2)

  categories = []
  for item2 in expense_list:
    category = item2
    if vert_size > len(item2):
      for i in range(vert_size - len(item2)):      
        category = category + ' '
    categories.append(category)
  #print(categories)
  
  for i in range(vert_size):
    stri = '    '
    
    for j in range(len(categories)):
      stri = stri + ' ' + categories[j][i]
    print(stri)  
